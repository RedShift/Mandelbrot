from distutils.core import setup

setup(name = "Mandelbrot",
      version = "1.0",
      description = "Mathematical tool for visualizing sections of the Mandelbrot set to arbitrary resolution.",
      author = "Troy P. Kling",
      author_email = "troykling1308@gmail.com",
      url = "https://gitlab.com/RedShift/Mandelbrot",
	  py_modules = ["mandelbrot"],
     )